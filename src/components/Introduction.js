import { Row, Col, Card } from "react-bootstrap";

export default function Introduction(){
	return (
		<Row className="my-3">
            <Col xs={12} md={8}>
                <Card className=" p-3 text-center intro-card">
                    <Card.Body>
                        <Card.Title className="mb-3">
                            <h2 className="header">#P r o d u c t O f T h e D a y</h2>
                        </Card.Title>
                        <img src = "../featured.png" width="550" height="450"/>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="p-3 text-center intro-card">
                    <Card.Body>
                        <Card.Title>
                            <h2 className="header mb-3">Hot Deals</h2>
                        </Card.Title>
                        <Card.Text>
                           <img src = "../products.gif" width="300" height="550"/>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
	)
}