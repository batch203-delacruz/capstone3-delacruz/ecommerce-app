

import { Card, Button, Row, Col, Container } from "react-bootstrap";

import { Link } from "react-router-dom";

export default function ProductCard({productProp}) {

	const { _id, productName, price, stock } = productProp;

	return (
		<Container fluid className="text-center">
			<Row className = "mb-3  h-100">
				<Col className = "my-5 card">
					<Card className="prod-card m-0 p-0 h-100 text-center">
					<img src = "../craftyclub_logo.png" width="250" height="250" className="mx-auto"/>

					    <Card.Body>

					        <Card.Title className=" mb-0">
					            {productName}
					        </Card.Title>
					        	
					    </Card.Body> 

					    <Card.Footer>
					    <Card.Subtitle >
					        		Price:
					        	</Card.Subtitle>
					        		<Card.Text className="mb-3">
					            	Php {price}.00
					        		</Card.Text>
					        	<Card.Subtitle className="mb-3">
					        		Stock: {stock}
					        	</Card.Subtitle>
					        <Button as = { Link } to = {`/products/${_id}`}variant="danger">More details</Button>
					    </Card.Footer>   
					</Card>
				</Col>
			</Row>
		</Container>	
	)
};