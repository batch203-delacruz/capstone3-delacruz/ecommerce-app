import { useState, useContext } from "react";
import { Container, Nav, Navbar, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import UserContext from "../UserContext";

export default function NavBar() {

  const {user} = useContext(UserContext);

  return (

    <Navbar id="nav" expand="lg">
      <Container>

        <Navbar.Brand as = { NavLink } to ="/"><img src = "../craftyclub_logo.png" width="50" height="50" /></Navbar.Brand>
        <Navbar.Brand  id ="brand-name" className="mx-3 prod-card">Crafty Club PH</Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className ="me-auto header"  >
            <Nav.Link as = { NavLink } to ="/" end className="header mx-3" id="link">Home</Nav.Link>

             {
                (user.isAdmin)
                ?
                <Nav.Link as = { NavLink } to ="/admin" end className="header mx-3 text-center" id="link">Admin Dashboard</Nav.Link>
                :
                <>
                <Nav.Link as = { NavLink } to ="/products" className="header mx-3" id="link">Products</Nav.Link>
                {/*<Nav.Link as = { NavLink } to ="/myOrders" end className="header mx-3" id="link">My Orders</Nav.Link>*/}
                </>

             }
          </Nav>

          <Nav>
             {
              (user.id !== null)
              ?
              <>
                
                <Nav.Link as = { NavLink } to ="/logout" end className="header mx-3" id="link">Logout</Nav.Link>
                
              </>
              :
              <>

                <Nav.Link as = { NavLink } to ="/login" end className="header mx-3" id="link">Login</Nav.Link>
                <Nav.Link as = { NavLink } to ="/register" end className="header mx-3" id="link">Register</Nav.Link>
              </>

             }  
          </Nav>

          {/*<Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-secondary">Search</Button>
          </Form>*/}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
