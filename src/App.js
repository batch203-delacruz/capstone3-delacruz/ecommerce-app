import { useState, useEffect } from "react";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap"

import './App.css';
import NavBar from "./components/NavBar";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Products from "./pages/Products";
import Logout from "./pages/Logout";
import AdminDashboard from "./pages/AdminDashboard";
import ProductView from "./pages/ProductView";
import MyOrders from "./pages/MyOrders";
import {UserProvider} from "./UserContext";


function App() {

const [user, setUser] = useState({
  id: null,
  isAdmin: null
});

const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {
  console.log(user);
  console.log(localStorage);
}, [user])

useEffect(() => {

  fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
    headers: {
      Authorization:`Bearer ${localStorage.getItem("token")}` 
    }
  })
  .then(res => res.json())
  .then(data => {
   

    if(data._id !==undefined) {

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    }
    else {
      setUser({
        id: null,
        isAdmin:null
      });
    }
  })  
}, [])

  return (

    <UserProvider value = {{user, setUser, unsetUser}}>

      <Router>
        <NavBar />

        <Container>
          <Routes>
            <Route exact path ="/" element={<Home />}/>
            <Route exact path ="/register" element={<Register />}/>
            <Route exact path ="/login" element={<Login />}/>
            <Route exact path ="/products" element={<Products />}/>
            <Route exact path ="/logout" element={<Logout />}/>
            <Route exact path ="/admin" element={<AdminDashboard />}/>
            <Route exact path ="/products/:productId" element={<ProductView />}/>
            <Route exact path ="/myOrders" element={<MyOrders />}/>

          </Routes>
        </Container>
      </Router>

    </UserProvider>
  );
}

export default App;
