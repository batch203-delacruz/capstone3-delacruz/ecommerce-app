import {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";

import {Navigate} from "react-router-dom";

import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([])

		const [productId, setProductId] = useState("");
		const [productName, setProductName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState(0);
	    const [stock, setStock] = useState(0);
	    const [userId, setUserId] = useState("")

	    
	    const [isAvailable, setIsAvailable] = useState(false);

	    // Add/Edit modal
	    const [showAdd, setShowAdd] = useState(false);
		const [showEdit, setShowEdit] = useState(false);
		const [showUpdateRole, setShowUpdateRole] = useState(false);

		// Add product modal
		const openAdd = () => setShowAdd(true); 
		const closeAdd = () => setShowAdd(false);

		// Edit product modal 
		const openEdit = (id) => {
			setProductId(id);

			fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setProductName(data.productName);
				setDescription(data.description);
				setPrice(data.price);
				setStock(data.stock);
			});

			setShowEdit(true)
		};

		const closeEdit = () => {

		    setProductName('');
		    setDescription('');
		    setPrice(0);
		    setStock(0);

			setShowEdit(false);
		};


	// 	// Update role modal 
	// 	const openToAdmin = (id, firstName) => {
	// 		setUserId(id);

	// 		fetch(`${ process.env.REACT_APP_API_URL }/users/updateRole/${id}`,{
	// 				method: "PATCH",
	// 				headers: {
	// 					"Content-Type": "application/json",
	// 					"Authorization": `Bearer ${localStorage.getItem("token")}`
	// 				},
	// 				body: JSON.stringify({
	// 					isAdmin: true
	// 				})
	// 			})
	// 		.then(res => res.json())
	// 		.then(data => {

	// 			console.log(data);

	// 			if(data){
	// 				Swal.fire({
	// 					title: "Update role succesful!",
	// 					icon: "success",
	// 					text: `${firstName} is now an admin.`
	// 				})
	// 			}
	// 			else{
	// 				Swal.fire({
	// 					title: "Cannot update role",
	// 					icon: "error",
	// 					text: "Something went wrong.Please try again later!"
	// 				})
	// 			}
	// 	})
	// }





	// View all products
	const fetchData = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stock}</td>
						<td>{product.isAvailable ? "Available" : "Unavailable"}</td>
						<td>
							{
								
								(product.isAvailable)
								?
									<Button variant ="danger" size = "sm" onClick ={() => archive(product._id, product.productName)}>Archive</Button>
								:
								<>
									<Button variant="dark" size ="sm" className ="mx-1 my-3" onClick ={() => unarchive(product._id, product.productName)}>Unarchive</Button>
									<Button variant="dark" size ="sm" className ="mx-1" onClick={() => openEdit(product._id)} >Edit</Button>
								</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	//View all users

	const fetchUsers = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				console.log(user);
				return (
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>						
						<td>{user.contactNumber}</td>
						<td>{user.address}</td>
						<td>{user.dateCreated}</td>
						<td>{user.isAdmin ? "Admin" : "Customer"}</td>
						<td>
							{
								
								(user.isAdmin)
								?
									<Button variant ="danger" size = "sm" onClick ={() => setCustomer(user._id, user.firstName)}>Set as Customer</Button>
								:
								
									<Button variant="dark" size ="sm" className ="mx-1 my-3" onClick ={() =>  setAdmin(user._id, user.firstName)}>Set as Admin</Button>
								

							}
						</td>
					</tr>
				)
			}));
		});
	}



	// To archive
	const archive = (id, productName) => {
		console.log(id);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`,{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAvailable: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful",
					icon: "error",
					text: "Something went wrong.Please try again later!"
				})
			}
		})
	}

	// To unarchive
	const unarchive = (id, productName) => {
		console.log(id);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`,{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAvailable: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				})
				
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong.Please try again later!"
				})
			}
		})
	}

	// To set as admin
	const setAdmin = (id, firstName) => {
		console.log(id);
		console.log(firstName);

		fetch(`${process.env.REACT_APP_API_URL}/users/updateRole/${id}`,{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Role updated",
					icon: "success",
					text: `${firstName} is now an admin.`
				})
				
				fetchUsers();
			}
			else{
				Swal.fire({
					title: "Cannot update role!",
					icon: "error",
					text: "Something went wrong.Please try again later!"
				})
			}
		})
	}


	// To set as customer
	const setCustomer = (id, firstName) => {
		console.log(id);
		console.log(firstName);

		fetch(`${process.env.REACT_APP_API_URL}/users/updateRole/${id}`,{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Role updated",
					icon: "success",
					text: `${firstName} is now a customer.`
				})
				
				fetchUsers();
			}
			else{
				Swal.fire({
					title: "Cannot update role!",
					icon: "error",
					text: "Something went wrong.Please try again later!"
				})
			}
		})
	}

	// Adding a product

	const addProduct = (e) =>{
			
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    productName: productName,
				    description: description,
				    price: price,
				    stock: stock
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully added!",
		    		    icon: "success",
		    		    text: `${productName} is now added`
		    		});
		    		
		    		fetchData();
		    		
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Cannot add product!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    setProductName('');
		    setDescription('');
		    setPrice(0);
		    setStock(0);
	}

	// Update a product
	
	const updateProduct = (e) =>{
			
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    productName: productName,
				    description: description,
				    price: price,
				    stock: stock
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully updated!",
		    		    icon: "success",
		    		    text: `${productName} is now updated.`
		    		});

		    		
		    		fetchData();
		    		
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Cannot update product!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    setProductName('');
		    setDescription('');
		    setPrice(0);
		    setStock(0);
	} 

	useEffect(() => {
       
        if(productName != "" && description != "" && price > 0 && stock > 0){
            setIsAvailable(true);
        } else {
            setIsAvailable(false);
        }

    }, [productName, description, price, stock]);


	useEffect(() => {
		fetchData();
	}, [])

	useEffect(() => {
		fetchUsers();
	}, [])


	return (
		
		(user.isAdmin)
		?
		<>
		<h1 className = "mt-5 mb-5 text-center header">Admin Dashboard</h1>
		<div className = "mt-5 mb-5 text-center">
			<Button variant ="danger" className="mx-auto w-100" onClick = {openAdd}>Add Product</Button>	
			{/*<Button variant="danger" className="mx-5 mb-5">Show Orders</Button>*/}
		</div>
		<Table striped bordered hover xs={12} md={8}>
			<thead>
				<tr className ="text-center">
				  <th>Product ID</th>
				  <th>Product Name</th>
				  <th>Description</th>
				  <th>Price</th>
				  <th>Stock</th>
				  <th>Status</th>
				  <th>Actions</th>
				</tr>
			</thead>
			<tbody className ="text-center">
				{allProducts}
			</tbody>
		</Table>


		{/*Modal for Adding a new product*/}
	        <Modal show={showAdd} fullscreen={false} onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="productName" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter product name" 
	    		                value = {productName}
	    		                onChange={e => setProductName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={5}
	    		                placeholder="Enter product description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter product price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="stock" className="mb-3">
	    	                <Form.Label>Stock</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter available stock" 
	    		                value = {stock}
	    		                onChange={e => setStock(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isAvailable 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>
	    		</Form>	
	    	</Modal>
	    

    	{/*Modal for Updating a product*/}
        <Modal show={showEdit} fullscreen={false} onHide={closeEdit}>
    		<Form onSubmit={e => updateProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="productName" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter product name" 
    		                value = {productName}
    		                onChange={e => setProductName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Product Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter product price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="stock" className="mb-3">
    	                <Form.Label>Stock</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter available stock" 
    		                value = {stock}
    		                onChange={e => setStock(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isAvailable 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>

    	<h1 className = "mt-5 mb-3 text-center header">All Users</h1>
		
		<Table striped bordered hover xs={12} md={8}>
			<thead>
				<tr className ="text-center">
				  <th>User ID</th>
				  <th>First Name</th>
				  <th>Last Name</th>
				  <th>Email</th>
				  <th>Contact Number</th>
				  <th>Address</th>
				  
				  <th>Date Created</th>
				  <th>Role</th>
				</tr>
			</thead>
			<tbody className ="text-center">
				{allUsers}
			</tbody>
		</Table>

			</>
		:
		<Navigate to ="/products"/>
	)
}