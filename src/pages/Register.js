import { useEffect, useState, useContext } from "react";

import { Form, Button, Container } from "react-bootstrap";

import { Navigate, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function Register(){

	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();


	const [fName, setFName] = useState('');
	const [lName, setLName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [contactNumber, setContactNumber] = useState('');
	const [address, setAddress] = useState('');

// Button(enable/disable)
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
 
		if((fName !== '' && lName !== '' && email !=='' && contactNumber !=='' && password1 !=='' && password2 !=='' && address !== '') && (password1 === password2)){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [fName, lName, email, password1, password2, contactNumber, address])



	function registerUser(e){

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data) {
				Swal.fire({
					title: "User already exists!",
					icon: "error",
					text: "Use a different email to register."
				})
			}
			else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: fName,
						lastName: lName,
						email: email,
						password: password1,
						contactNumber: contactNumber,
						address: address
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

						if(data) {
							Swal.fire({
								title: "Registration is successful!",
								icon: "success",
								text: "Welcome to Crafy Club PH!"
							});
								setFName('');
								setLName('');
								setEmail('');
								setContactNumber('');
								setPassword1('');
								setPassword2('');
								setAddress('');

								navigate("/login")
						}
						else{
							Swal.fire({
								title: "Oops! Something went wrong",
								icon: "error",
								text: "Please try again."
							});
						}		
					});
				}
		})
	}

	return (

	<>
	
		<Container className="d-flex my-5" id="container-register">

		<Form onSubmit ={e => registerUser(e)} className = "col-12 col-md-6">

		<h1 className="my-3 text-center header">Sign Up</h1>
			<Form.Group className="mx-3 my-3">
				<Form.Label>First Name</Form.Label>
				<Form.Control
				type = "text"
				placeholder = "Enter your first name"
				value = {fName}
				onChange={e => setFName(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group className="mx-3 my-3">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
				type = "text"
				placeholder = "Enter your last name"
				value = {lName}
				onChange={e => setLName(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group className="mx-3 my-3">
			<Form.Label>Email</Form.Label>
				<Form.Control
				type = "email"
				placeholder = "Enter a valid email"
				value = {email}
				onChange={e => setEmail(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group className="mx-3 my-3">
			<Form.Label>Password</Form.Label>
				<Form.Control
				type = "password"
				placeholder = "Enter your password"
				value = {password1}
				onChange={e => setPassword1(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group className="mx-3 my-3">
			<Form.Label>Verify Password</Form.Label>
				<Form.Control
				type = "password"
				placeholder = "Confirm your password"
				onChange={e => setPassword2(e.target.value)}
				value = {password2}
				required
				/>
			</Form.Group>

			<Form.Group className="mx-3 my-3">
			<Form.Label>Mobile Number</Form.Label>
				<Form.Control
				type = "text"
				placeholder = "Enter your active mobile number"
				value = {contactNumber}
				onChange={e => setContactNumber(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group className="mx-3 my-3">
			<Form.Label>Address</Form.Label>
				<Form.Control
				type = "text"
				placeholder = "Enter your complete address"
				value = {address}
				onChange={e => setAddress(e.target.value)}
				required
				/>
			</Form.Group>
			<p className = "text-center">Already have an account? Login <a href="/login" className="text-dark">here.</a></p>


			{
				isActive
				?
				<Container className = "text-center mb-5">
				<Button className = "m-auto"  variant="danger" type="submit" id="submitBtn">Sign Up</Button>
				</Container>

				:
				<Container className = "text-center mb-5">
				<Button className = "m-auto text-center"  variant="danger" type="submit" id="submitBtn" disabled>Sign Up</Button>
				</Container>


			}
			
		</Form>
		
		<img src = "../craftyclub_logo.png" width="300" height="300" className="m-auto img-fluid prod-card "/>
		
		</Container>
	</>
	)
}