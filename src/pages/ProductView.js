import { useState, useEffect, useContext } from "react";

import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";

import Swal from "sweetalert2";

import UserContext from "../UserContext";

import { useParams, useNavigate, Link } from "react-router-dom";

export default function ProductView(){

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const [ productName, setProductName] = useState('');
	const [ description, setDescription] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ stock, setStock ] = useState(0);
	const [ quantity, setQuantity] = useState(1);
	

	
	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setStock(data.stock);
			
		})
	}, [productId])

	const checkout = (productId) => {
		console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				products:[
				{
					productId:productId,
					productName: productName,
					quantity: quantity
				}],
				totalAmount: price * quantity

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Item successfully checked out!",
					icon: "success",
				});

				navigate("/products");
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})	
	}

	return (
		<Container fluid>
			<Row className="my-5 justify-content-center">
				<Col >
					<Card id="card-view" className="mx-auto">
					    <Card.Body className="text-center">
					    <img src = "../craftyclub_logo.png" width="250" height="250" className="mx-auto"/>
					        <Card.Title className="my-3">{productName}</Card.Title>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle>Stock</Card.Subtitle>
					        <Card.Text>{stock}</Card.Text>
					        <Card.Text></Card.Text>
					        
					        {

					        	user.id !== null
					        	
					        	?
					        	<>
					        		<Form.Group className="mb-3" controlId="quantity">
				        		          <Form.Label>Quantity</Form.Label>
				        		          <Form.Control 
				        		          type="number" 
				        		          placeholder="Enter quantity of order" 
				        		          value = {quantity}
				        		          onChange = {e => setQuantity(e.target.value)}
				        		          required
				        		          />
			        		        </Form.Group>

					        		<Button variant="danger" size="lg" onClick={() => checkout(productId)}>Buy Now</Button>
								</>
					        	:
					        		<Button as = {Link} to ="/login" variant="danger" size="lg">Login to Buy</Button>
					        }
					    </Card.Body>        
					</Card>
				</Col>
			</Row>
		</Container>

	)
}