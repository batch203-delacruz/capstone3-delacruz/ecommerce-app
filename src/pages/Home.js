import Introduction from "../components/Introduction";

export default function Home(){
	return (

		<>
		<img src = "../banner_crafty.gif" className="my-5 mx-5"/>
		<Introduction />
		</>
	)
}